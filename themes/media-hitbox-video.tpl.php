<?php

/**
 * @file media_hitbox/themes/media-hitbox-video.tpl.php
 *
 * Template file for theme('media_hitbox_video').
 *
 * Variables available:
 *  $uri - The media uri for the Hitbox video (e.g., hitbox://v/51951651).
 *  $video_id - The unique identifier of the Hitbox video (e.g., 51951651).
 *  $id - The file entity ID (fid).
 *  $url - The full url including query options for the Hitbox iframe.
 *  $options - An array containing the Media Hitbox formatter options.
 *  $api_id_attribute - An id attribute if the Javascript API is enabled;
 *  otherwise NULL.
 *  $width - The width value set in Media: Hitbox file display options.
 *  $height - The height value set in Media: Hitbox file display options.
 *  $title - The Media: YouTube file's title.
 *  $alternative_content - Text to display for browsers that don't support
 *  iframes.
 *
 */

?>
<div class="<?php print $classes; ?> media-hitbox-<?php print $id; ?>">
  <iframe class="media-hitbox-player" <?php print $api_id_attribute; ?>width="<?php print $width; ?>" height="<?php print $height; ?>" title="<?php print $title; ?>" src="<?php print $url; ?>" frameborder="0" allowfullscreen><?php print $alternative_content; ?></iframe>
</div>