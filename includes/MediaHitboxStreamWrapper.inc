<?php

/**
 *  @file
 *  Extends the MediaReadOnlyStreamWrapper class to handle Hitbox videos.
 */

/**
 *  Create an instance like this:
 *  $hitbox = new MediaHitboxStreamWrapper('hitbox://v/[video-code]');
 */
class MediaHitboxStreamWrapper extends MediaReadOnlyStreamWrapper {
  protected $base_url = 'http://www.hitbox.tv';

  static function getMimeType($uri, $mapping = NULL) {
    return 'video/hitbox';
  }

  function interpolateUrl() {
    if ($parameters = $this->get_parameters()) {
      return $this->base_url . '/video/' . $parameters['v'];
    }
  }

  function getOriginalThumbnailPath() {
    return "";
  }

  function getLocalThumbnailPath() {
    return "";
  }
}
